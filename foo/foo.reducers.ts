import {Action} from 'redux';
import {isType} from 'typescript-fsa';
import { setBar } from "./foo.actions";

export interface IFooState {
    bar: string;
}

const initialState: IFooState = {
    bar: "loading",
}

export function fooReducer(state = initialState, action: Action): IFooState {
    if (isType(action, setBar)) {
        return Object.assign({}, state, {
            bar: action.payload,
        });
    }

    return state;
}