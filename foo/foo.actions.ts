import actionCreatorFactory from 'typescript-fsa';

const actionCreator = actionCreatorFactory('Foo');

export const setBar = actionCreator<string>('SET_BAR');