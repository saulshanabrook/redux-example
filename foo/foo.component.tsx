import * as React from "react";
import { Component } from "react";

interface IFooProps {
    bar: string;
    doSomething: () => void;
}

export class FooComponent extends Component<IFooProps, {}> {
    render() {
        return (
            <div>
                {this.props.bar}
                <br/>
                <button onClick={this.props.doSomething}>Click Me</button>
            </div>
        )
    }
}
