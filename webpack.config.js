const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const APP_DIR = path.resolve(__dirname, '');
let BUILD_DIR;
BUILD_DIR = path.resolve(__dirname, 'bundles/');


// Rules
let rules = [
    {
        test: /\.tsx?$/,
        include: APP_DIR,
        loader: 'awesome-typescript-loader',
        enforce: 'pre',
    },
    {
        test: /\.[tj]sx?/,
        include: APP_DIR,
        loader: 'babel-loader',
        query: {
            presets: ['es2015']
        }
    }
];


// Transformation Plugins
let plugins = [
];



// Vendor Bundle
let vendorPackages = [
    'babel-polyfill',
    'react',
    'react-redux',
    'redux',
];


// Application Bundle
// Use dev server when not in production
let app = [
    'webpack-dev-server/client?http://docker-machine.local:8080',
    'webpack/hot/only-dev-server',
    path.join(APP_DIR, 'index.tsx')
];


// Output
let output = {
    path: BUILD_DIR,
    filename: '[name].js'
};


let config = {
    resolve: {
        modules: ['node_modules'],
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"],
    },
    entry: {
        vendor: vendorPackages,
        app: app

    },
    output: output,
    plugins: plugins,
    module: {
        rules: rules
    },
    devtool: 'eval-cheap-module-source-map',
    devServer: {
        hot: true,
        inline: true,
        compress: true,
        disableHostCheck: true
    }
};

module.exports = config;
