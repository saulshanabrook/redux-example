import { combineReducers } from "redux";
import { createSelector } from 'reselect'

import { fooReducer, IFooState } from "./foo"

export interface IState {
    foo: IFooState;
}

export const rootReducer = combineReducers({
    foo: fooReducer,
})

export const getFooState = (state: IState) => state.foo;
export const getBar = createSelector(
    getFooState,
    (fooState) => fooState.bar
);
